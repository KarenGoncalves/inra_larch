files = list()
for (i in replicates ){ files[[i]] = list.files(path = paste0(i, '.stat/'), pattern = 'genes.results')}

counts_files = list()
for (i in replicates ){ counts_files[[i]] = read.delim(paste0(i, '.stat/', files[[i]]))}

genes_matrix = data.frame(
gene_id = counts_files[[i]]$gene_id, 
counts_files[[1]]$expected_count, counts_files[[2]]$expected_count, 
counts_files[[3]]$expected_count, counts_files[[4]]$expected_count, 
counts_files[[5]]$expected_count, counts_files[[6]]$expected_count)
colnames(genes_matrix)[2:7] = replicates 

write.table(genes_matrix, 'genes_matrix_counts.txt', sep = '\t')

## DESeq2 FOR GENES

counts = read.delim('genes_matrix_counts.txt')
rownames(counts) = counts$gene_id
counts$gene_id = NULL

files = list()
for (i in replicates ){ files[[i]] = list.files(path = paste0(i, '.stat/'), pattern = 'genes.results')}
length = read.delim(paste0(i, '.stat/', files[[i]]))
length = length$length
names(length) = rownames(counts)

tpm = Counts_to_tpm(counts = counts, featureLength = length)[[1]]
cutv = DAFS(tpm=tpm)
refs = gene_selection(countsToTpm_result= tpm, dafs_result=cutv)
counts_filtered = counts[apply(tpm, 1, mean) > (mean(cutv)^2),]
design = data.frame(Samples = c(rep('Aecia', 3), rep('Spermogonia', 3)), Replicates = replicates )
for (i in 1:ncol(counts_filterd)){ counts_filterd[, i] = as.integer(counts_filterd[, i]) }

dds = DESeqDataSetFromMatrix(countData = counts_filterd, colData = design, design = ~Samples)
references = rownames(counts_filterd) %in% rownames(refs)
dds = estimateSizeFactors(dds, controlGenes = which(references))
dds = DESeq(dds)
result_dds = results(object = dds, name = "Samples_Spermogonia_vs_Aecia")
write.table(result_dds, 'Complete_results_PicnavsAecia.txt', row.names = T, col.names=T, sep = '\t', quote=F)
filtering = result_dds[!is.na(result_dds$padj), ]
result_filtering = filtering[which(filtering$padj < 0.05 & abs(filtering$log2FoldChange) > 2),]
write.table(result_filtering, 'Filtered_results_PicnavsAecia.txt', row.names = T, col.names=T, sep = '\t', quote=F)

rlog = rlogTransformation(dds)
pca = plotPCA(rlog, intgroup = 'Samples', ntop = nrow(dds)) + theme_bw()
pca$labels$colour = 'Growth stage'
pca
ggsave('PCA_Picnia_Aecia.pdf')
ggsave('PCA_Picnia_Aecia.tiff')
